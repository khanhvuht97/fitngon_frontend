export const environment = {
  production: false,
  jwtTokenKey: 'JWT_TOKEN',
  apiUrl: 'http://fitngon_api.local'
};
