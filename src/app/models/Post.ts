import Model from './Model';
import PostCategory from './PostCategory';
import * as _ from 'lodash';

class Post extends Model {
  postCategory: any;
  constructor(options) {
    super();
    this.postCategory = d => {
      return new PostCategory(d.data);
    };
    this.bind(options);
  }
}

export default Post;
