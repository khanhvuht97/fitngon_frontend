import Model from './Model';

class PostCategory extends Model {
  constructor(options) {
    super();
    this.bind(options);
  }
}

export default PostCategory;
