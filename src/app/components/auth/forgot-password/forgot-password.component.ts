import { Component, OnInit } from '@angular/core';
import { Store } from './../../../store/store.module';
import { FORGOT_PASSWORD_REQUESTED } from './forgot-password.actions';
import Notification from '@vicoders/support/services/Notification';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  public store;
  public email = '';
  public data: any = {
    email: ''
  };
  constructor(store: Store) {
    this.store = store.getInstance();
  }

  ngOnInit() {}

  onSubmit() {
    if (this.data.email === '') {
      Notification.show('warning', 'Email is required', 3000);
      return false;
    }
    this.store.dispatch({ type: FORGOT_PASSWORD_REQUESTED, data: this.data });
  }
}
