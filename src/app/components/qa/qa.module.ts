import { DetailComponent } from './detail/detail.component';
import { ListComponent } from './list/list.component';
import { QaRoutingModule } from './qa-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [QaRoutingModule, CommonModule],
  declarations: [DetailComponent, ListComponent, ]
})
export class QaModule { }

