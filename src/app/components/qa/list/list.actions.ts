export const FETCH_LIST_QA_LIST_REQUESTED = 'FETCH_LIST_QA_LIST_REQUESTED';
export const FETCH_LIST_QA_LIST_SUCCESSED = 'FETCH_LIST_QA_LIST_SUCCESSED';

export const fetchQaListRequested = (payload?) => {
    payload = payload || {};
    return {
        type: FETCH_LIST_QA_LIST_REQUESTED,
        data: payload
    };
};

export const fetchQaListSuccessed = (payload?) => {
    payload = payload || {};
    return {
        type: FETCH_LIST_QA_LIST_SUCCESSED,
        data: payload
    };
};
