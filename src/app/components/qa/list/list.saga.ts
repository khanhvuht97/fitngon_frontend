import { put, takeLatest } from 'redux-saga/effects';
import { FETCH_LIST_QA_LIST_REQUESTED, fetchQaListSuccessed } from './list.actions';
import { AppInjector } from '../../../app-injector';
import { ApiService } from '../../../api/api.service';
import { API_CALL_ERROR } from '../../../store/action';

function* watchFetchListListRequest() {
  yield takeLatest(FETCH_LIST_QA_LIST_REQUESTED, function*(action: any) {
    //   const router = AppInjector.get(Router);
    try {
      const result = yield AppInjector.get(ApiService)
        .qa.get(action.data)
        .toPromise();
      yield put(fetchQaListSuccessed({ items: result.items, pagination: result.pagination }));
      // router.navigate(listQaList());
    } catch (e) {
      yield put({ type: API_CALL_ERROR, error: e });
    }
  });
}

export default [watchFetchListListRequest];
