import * as _ from 'lodash';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { BaseComponent } from '../../base.component';
import { fetchQaListRequested } from './list.actions';
import { qaDetailPageRouter } from '../qa.const';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends BaseComponent implements OnInit {
  public reducer: String = 'Qa.List';
  public qaDetailPageRouter = qaDetailPageRouter;

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {
    super();
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        let params = this.queryParser.parse(['search'], activatedRoute);
        params = _.assign(this.queryParser.parse(['search', 'per_page', 'page']), { include: 'postCategory' });
        this.store.dispatch(fetchQaListRequested(params));
      }
    });
  }

  ngOnInit() {
    this.init();
  }

  mapStateToProps(state) {
    return {
      payload: state.Qa.List
    };
  }

  mapDispatchToProps(dispatch) {
    return {
      dispatch
    };
  }
}
