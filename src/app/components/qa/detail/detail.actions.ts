export const FETCH_DATA_QA_DETAIL_REQUESTED = 'FETCH_DATA_QA_DETAIL_REQUESTED';
export const FETCH_DATA_QA_DETAIL_SUCCESSED = 'FETCH_DATA_QA_DETAIL_SUCCESSED';


export const fetchQaDetailRequested = (payload) => {
    return {
        type: FETCH_DATA_QA_DETAIL_REQUESTED,
        data: payload,
    };
};
