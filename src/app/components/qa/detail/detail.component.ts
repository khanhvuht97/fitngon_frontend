import * as _ from 'lodash';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { BaseComponent } from '../../base.component';
import { fetchQaDetailRequested } from './detail.actions';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent extends BaseComponent implements OnInit {
  public reducer: String = 'Qa.Detail';
  private activatedRoute;

  constructor(activatedRoute: ActivatedRoute, private router: Router) {
    super();
    this.activatedRoute = activatedRoute;
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        const id = this.activatedRoute.snapshot.params.id;
        this.dispatch(fetchQaDetailRequested({id: id}));
      }
    });
  }

  ngOnInit() {
    this.init();
  }

  mapStateToProps(state) {
    return {
      payload: state.Qa.Detail
    };
  }

  mapDispatchToProps(dispatch) {
    return {
      dispatch
    };
  }
}
