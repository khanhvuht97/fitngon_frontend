import * as _ from 'lodash';
import { FETCH_DATA_QA_DETAIL_SUCCESSED } from './detail.actions';

export const Detail = (state = {fetched: false, item: {}}, action) => {
  switch (action.type) {
    case FETCH_DATA_QA_DETAIL_SUCCESSED:
        return _.assign(state, {fetched: true, item: action.data});
    default:
      return state;
  }
};
