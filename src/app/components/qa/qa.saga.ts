import Detail from './detail/detail.saga';
import List from './list/list.saga';
import * as _ from 'lodash';
import { put, select, takeLatest, call } from 'redux-saga/effects';
import { GET_ALL_QA_REQUESTED, GET_ALL_QA_SUCCESSED } from './qa.actions';
import { AppInjector } from '../../app-injector';
import { ApiService } from '../../api/api.service';
import { API_CALL_ERROR } from '../../store/action';


export function* fetchAllQas() {
    const fetched = yield select((state) => (state as any).Qa.all.fetched);
    if (!fetched) {
      try {
        const api = AppInjector.get(ApiService);
        let result = yield api.qa.list().toPromise();
        return result;
      } catch (e) {
        yield put({ type: API_CALL_ERROR, error: e });
      }
    } else {
      const data = yield select(state => (state as any).Qa.all.items);
      return data;
    }
  }

  function* watchFetchAllQasRequest() {
    yield takeLatest(GET_ALL_QA_REQUESTED, function* (action:any) {
      const data = yield call(fetchAllQas);
      yield put({type: GET_ALL_QA_SUCCESSED, data: data});
    });
  }


export default [...Detail, ...List, watchFetchAllQasRequest];
