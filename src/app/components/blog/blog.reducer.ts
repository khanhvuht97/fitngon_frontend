import { Detail } from './detail/detail.reducer';
import { List } from './list/list.reducer';
import { combineReducers } from 'redux';
import { GET_ALL_BLOG_SUCCESSED } from './blog.actions';
import * as _ from 'lodash';

const all = (state = { fetched: false, items: []}, action) => {
  switch (action.type) {
    case GET_ALL_BLOG_SUCCESSED:
      return _.assign({}, state, {
        fetched: true,
        items: [...action.data]
      });
    default:
      return state;
  }
};

export const Blog = combineReducers({Detail, List,  all });
