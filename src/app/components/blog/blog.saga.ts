import Detail from './detail/detail.saga';
import List from './list/list.saga';
import * as _ from 'lodash';
import { put, select, takeLatest, call } from 'redux-saga/effects';
import { GET_ALL_BLOG_REQUESTED, GET_ALL_BLOG_SUCCESSED } from './blog.actions';
import { AppInjector } from '../../app-injector';
import { ApiService } from '../../api/api.service';
import { API_CALL_ERROR } from '../../store/action';

export function* fetchAllBlogs() {
  const fetched = yield select(state => (state as any).Blog.all.fetched);
  if (!fetched) {
    try {
      const api = AppInjector.get(ApiService);
      const result = yield api.post.list().toPromise();
      return result;
    } catch (e) {
      yield put({ type: API_CALL_ERROR, error: e });
    }
  } else {
    const data = yield select(state => (state as any).Blog.all.items);
    return data;
  }
}

function* watchFetchAllBlogsRequest() {
  yield takeLatest(GET_ALL_BLOG_REQUESTED, function*(action: any) {
    const data = yield call(fetchAllBlogs);
    yield put({ type: GET_ALL_BLOG_SUCCESSED, data: data });
  });
}

export default [...Detail, ...List, watchFetchAllBlogsRequest];
