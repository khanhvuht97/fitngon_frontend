import { put, takeLatest } from 'redux-saga/effects';
import { FETCH_LIST_BLOG_LIST_REQUESTED, fetchBlogListSuccessed } from './list.actions';
import { AppInjector } from '../../../app-injector';
import { ApiService } from '../../../api/api.service';
import { API_CALL_ERROR } from '../../../store/action';

function* watchFetchListListRequest() {
    yield takeLatest(FETCH_LIST_BLOG_LIST_REQUESTED, function* (action: any) {
    //   const router = AppInjector.get(Router);
      try {
        const result = yield AppInjector.get(ApiService).post.get(action.data).toPromise();
        yield put(fetchBlogListSuccessed({items: result.items, pagination: result.pagination}));
        // router.navigate(listBlogList());
      } catch (e) {
        yield put({ type: API_CALL_ERROR, error: e });
      }
    });
  }

  export default [watchFetchListListRequest];
