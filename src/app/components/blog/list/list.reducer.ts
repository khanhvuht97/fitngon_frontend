import { FETCH_LIST_BLOG_LIST_SUCCESSED } from './list.actions';
import * as _ from 'lodash';

export const List = (state = {fetched: false, items: []}, action) => {
  switch (action.type) {
    case FETCH_LIST_BLOG_LIST_SUCCESSED:
      return _.assign({}, state, { fetched: true, items: action.data.items, pagination: action.data.pagination });
    default:
      return state;
  }
};
