export const FETCH_LIST_BLOG_LIST_REQUESTED = 'FETCH_LIST_BLOG_LIST_REQUESTED';
export const FETCH_LIST_BLOG_LIST_SUCCESSED = 'FETCH_LIST_BLOG_LIST_SUCCESSED';

export const fetchBlogListRequested = (payload?) => {
    payload = payload || {};
    return {
        type: FETCH_LIST_BLOG_LIST_REQUESTED,
        data: payload
    };
};

export const fetchBlogListSuccessed = (payload?) => {
    payload = payload || {};
    return {
        type: FETCH_LIST_BLOG_LIST_SUCCESSED,
        data: payload
    };
};
