import { DetailComponent } from './detail/detail.component';
import { ListComponent } from './list/list.component';
import { BlogRoutingModule } from './blog-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DirectivesModule } from '@vicoders/angular';

@NgModule({
  imports: [BlogRoutingModule, CommonModule, DirectivesModule],
  declarations: [DetailComponent, ListComponent]
})
export class BlogModule {}
