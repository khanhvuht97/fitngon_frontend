import { put, takeLatest } from 'redux-saga/effects';
import { FETCH_DATA_BLOG_DETAIL_REQUESTED, FETCH_DATA_BLOG_DETAIL_SUCCESSED } from './detail.actions';
import { AppInjector } from '../../../app-injector';
import { ApiService } from '../../../api/api.service';
import { API_CALL_ERROR } from '../../../store/action';

function* watchCreateDetailRequest() {
    yield takeLatest(FETCH_DATA_BLOG_DETAIL_REQUESTED, function* (action: any) {
      try {
        const result = yield AppInjector.get(ApiService).post.show(action.data.id).toPromise();
        yield put({ type: FETCH_DATA_BLOG_DETAIL_SUCCESSED, data: result });
      } catch (e) {
        yield put({ type: API_CALL_ERROR, error: e });
      }
    });
  }

  export default [watchCreateDetailRequest];
