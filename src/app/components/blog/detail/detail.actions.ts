export const FETCH_DATA_BLOG_DETAIL_REQUESTED = 'FETCH_DATA_BLOG_DETAIL_REQUESTED';
export const FETCH_DATA_BLOG_DETAIL_SUCCESSED = 'FETCH_DATA_BLOG_DETAIL_SUCCESSED';


export const fetchBlogDetailRequested = (payload) => {
    return {
        type: FETCH_DATA_BLOG_DETAIL_REQUESTED,
        data: payload,
    };
};
