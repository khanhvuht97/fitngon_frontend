import { Injectable } from '@angular/core';
import Qa from '../../models/Qa';
import { BaseService } from '../base.service';

@Injectable()
export class QaService extends BaseService {
  public url = '/api/qas';
  public model = Qa;
}


