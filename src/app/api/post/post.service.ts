import { Injectable } from '@angular/core';
import Post from '../../models/Post';
import { BaseService } from '../base.service';

@Injectable()
export class PostService extends BaseService {
  public url = '/api/posts';
  public model = Post;
}
