import Qa from '../components/qa/qa.saga';
import Blog from '../components/blog/blog.saga';
import Homepage from '../components/homepage/homepage.saga';
import { takeEvery, fork, all } from 'redux-saga/effects';
import main from '../components/main.saga';
import auth from '../components/auth/auth.saga';
import { API_CALL_ERROR } from './action';
import Loader from '@vicoders/support/services/Loader';
import Notification from '@vicoders/support/services/Notification';

function* watchApiCallError() {
  yield takeEvery(API_CALL_ERROR, function*(action) {
    Loader.hide();
    if ((action as any).error !== undefined) {
      if ((action as any).error.error !== undefined && (action as any).error.error.message !== undefined) {
        Notification.show('warning', (action as any).error.error.message, 5000);
      }
    }
  });
}

export default function* sagas() {
  yield all([...Qa, ...Blog, ...Homepage, ...main, ...auth, watchApiCallError].map(item => fork(item)));
}
