// Uncomment the next line if you want to use bootstrap, don't forget uncomment jQuery defination in webpack.common.js line 93
import 'jquery';
import 'bootstrap';
import 'slick-carousel';
$(document).ready(function(){
    $('.melancholy').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows: false,
        centerMode: true,
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2.5,
                    slidesToScroll: 1,
                    infinite: true,
                }
            },
            {
                breakpoint: 992,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
        ]
    });
    $('.partner__content').slick({
        infinite: true,
        slidesToShow: 6,
        slidesToScroll: 6,
        arrows: true,
        centerMode: true,
        focusOnSelect: true,
        prevArrow:"<button class='pre'></button>",
        nextArrow:"<button class='next'></button>",
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 5,
                    infinite: true,
                }
            },
            {
                breakpoint: 992,
                settings: {
                  slidesToShow: 4,
                  slidesToScroll: 4
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 375,
                settings: {
                    slidesToShow: 1.5,
                    slidesToScroll: 1.5
                }
            },
            {
                breakpoint: 350,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
        ]
    });

    //details slide

    $('.img__main').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.img__child'
    });
    
    $('.img__child').slick({
        slidesToShow: 4,
        infinite: true,
        autoplay:true,
        arrows: false,
        asNavFor: '.img__main',
        focusOnSelect: true
      
    });
     

    //header-search
    $(".toggle-overlay").click(function(){
        $(".search-body").toggleClass("search-open");
    });

    //menu
    var path = window.location.href;
    $('.main-menu>li a').each(function(){
        if (this.href === path) {
            $(this).parent().addClass('active');
        }
    });
    
});